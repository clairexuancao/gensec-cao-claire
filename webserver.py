
#Code

from flask import Flask, request, jsonify

app = Flask(__name__)
# Define a simple endpoint
@app.route('/hello', methods=['GET'])
def hello():
    return 'Hello, World!'

# Define a simple endpoint
@app.route('/chat', methods=['POST'])
def chat():
    print(request.json)
    if  request.json['question'] == "is sushi a good cat?" :
        return "meow. neither confirm nor deny"
    else:
        return 'sushi poooped!'
    

# Define an endpoint that returns JSON data
@app.route('/data', methods=['GET'])
def get_data():
    data = {
        'name': 'John Doe',
        'age': 30,
        'city': 'New York'
    }
    return jsonify(data)

if __name__ == '__main__':
    app.run(host='34.82.154.28', port=6000, debug=True)
