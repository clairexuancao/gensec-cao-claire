from langchain_google_genai import GoogleGenerativeAI
from langchain import hub
from langchain.chains import LLMMathChain
from langchain.agents import AgentExecutor, create_react_agent, load_tools
from langchain_experimental.tools import PythonREPLTool

llm = GoogleGenerativeAI(model="gemini-1.5-pro-latest",temperature=0)

tools = load_tools(["serpapi", "llm-math","wikipedia","terminal"],
llm=llm, allow_dangerous_tools=True)
llm_math= LLMMathChain.from_llm(llm)
tools.extend([PythonREPLTool()])

#RAGApp Code
#loaddb
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.document_loaders import AsyncHtmlLoader, DirectoryLoader, TextLoader, PyPDFDirectoryLoader, Docx2txtLoader, UnstructuredMarkdownLoader, WikipediaLoader, ArxivLoader, CSVLoader
from langchain_community.vectorstores import Chroma
from langchain_google_genai import GoogleGenerativeAIEmbeddings
from langchain_community.document_transformers import BeautifulSoupTransformer
import readline

vectorstore = Chroma(
    embedding_function=GoogleGenerativeAIEmbeddings(model="models/embedding-001", task_type="retrieval_query"),
    persist_directory="./rag_data/.chromadb"
)

def load_docs(docs):
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=10000, chunk_overlap=10)
    splits = text_splitter.split_documents(docs)
    vectorstore.add_documents(documents=splits)

def load_urls(urls):
    load_docs(AsyncHtmlLoader(urls).load())

def load_wikipedia(query):
    load_docs(WikipediaLoader(query=query, load_max_docs=1).load())

def load_arxiv(query):
    docs = ArxivLoader(query=query, load_max_docs=1).load()
    docs[0].metadata['source'] = f"arxiv:{query}"
    load_docs(docs)

def load_txt(directory):
    load_docs(DirectoryLoader(directory, glob="**/*.txt", loader_cls=TextLoader).load())

def load_pdf(directory):
    load_docs(PyPDFDirectoryLoader(directory).load())

def load_docx(directory):
    load_docs(DirectoryLoader(directory, glob="**/*.docx", loader_cls=Docx2txtLoader).load())

def load_md(directory):
    load_docs(DirectoryLoader(directory, glob="**/*.md", loader_cls=UnstructuredMarkdownLoader).load())

def load_csv(directory):
    load_docs(DirectoryLoader(directory, glob="**/*.csv", loader_cls=CSVLoader).load())

urls = ["https://www.multco.us/dd/shelter-and-homeless-services", "https://rosecityresource.streetroots.org","https://blanchethouse.org/ourservices/",
"https://www.211info.org/","https://johs.us/find-help/","https://johs.us/emergency-shelters/list-of-shelters/", "https://www.pdx.edu/homelessness/find-help",
"https://portlandrescuemission.org/our-services/", "https://www.oregon.gov/ohcs/for-providers/Pages/homeless-services-programs.aspx"
]
print(f"Loading: {urls}")
load_urls(urls)

wiki_query = "Homelessness in Oregon"
print(f"Loading Wikipedia pages on: {wiki_query}")
load_wikipedia(wiki_query)

#arxiv_query = "2310.03714"
#print(f"Loading arxiv document: {arxiv_query}")
#load_arxiv(arxiv_query)
#text_directory = "rag_data/txt"
#print(f"Loading TXT files from: {text_directory}")
#load_txt(text_directory)

pdf_directory = "/home/clairexuancao/gensec-Cao-Claire/hwl/pdf_data"
print(f"Loading PDF files from: {pdf_directory}")
load_pdf(pdf_directory)

import fitz  # PyMuPDF

# Open the PDF file
#pdf_document = fitz.open("/home/clairexuancao/gensec-Cao-Claire/hwl/pdf_data/Portland Area Resources.pdf")

# Loop through each page
#for page_num in range(len(pdf_document)):
 #   page = pdf_document.load_page(page_num)
  #  text = page.get_text("text")  # Extract text
   # print(f"Page {page_num + 1}:\n{text}\n")

#docx_directory = "rag_data/docx"
#print(f"Loading DOCX files from: {docx_directory}")
#load_docx(docx_directory)

#md_directory = "rag_data/md"
#print(f"Loading MD files from: {md_directory}")
#load_md(md_directory)

#csv_directory = "rag_data/csv"
#print(f"Loading CSV files from: {csv_directory}")
#load_csv(csv_directory)

print("RAG database initialized with the following sources.")
retriever = vectorstore.as_retriever()
document_data_sources = set()
for doc_metadata in retriever.vectorstore.get()['metadatas']:
    document_data_sources.add(doc_metadata['source']) 
for doc in document_data_sources:
    print(f"  {doc}")
#query

from langchain import hub
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.vectorstores import Chroma
from langchain_core.output_parsers import StrOutputParser
from langchain_core.runnables import RunnablePassthrough
from langchain_google_genai import GoogleGenerativeAI, GoogleGenerativeAIEmbeddings
import readline

vectorstore = Chroma(
     persist_directory="./rag_data/.chromadb",
     embedding_function=GoogleGenerativeAIEmbeddings(model="models/embedding-001", task_type="retrieval_query")
)

retriever = vectorstore.as_retriever()

prompt = hub.pull("rlm/rag-prompt")

llm = GoogleGenerativeAI(model="gemini-1.5-pro",temperature=0)

def format_docs(docs):
    return "\n\n".join(doc.page_content for doc in docs)

rag_chain = (
    {"context": retriever | format_docs, "question": RunnablePassthrough()}
    | prompt
    | llm
    | StrOutputParser()
)

print("Welcome to my RAG application.  Ask me a question and I will answer it from the documents in my database shown below")
# Iterate over documents and dump metadata
document_data_sources = set()
for doc_metadata in retriever.vectorstore.get()['metadatas']:
    document_data_sources.add(doc_metadata['source']) 
for doc in document_data_sources:
    print(f"  {doc}")

#while True:
#    line = input("llm>> ")
#    if line:
#        result = rag_chain.invoke(line)
#        print(result)
#    else:
#        break

# Create the RAG chain
rag_chain = (
    {"context": retriever | format_docs, "question": RunnablePassthrough()}
    | prompt
    | llm
    | StrOutputParser()
)

# RAG application function
def RAGapplication(query):
    return rag_chain.invoke(query)

# Example usage
#answer = RAGapplication("Where is Blanchet House?")
#print(answer)


base_prompt = hub.pull("langchain-ai/react-agent-template")
prompt = base_prompt.partial(instructions=
"Answer the user's request utilizing at most 8 tool calls")
agent = create_react_agent(llm,tools,prompt)
agent_executor = AgentExecutor(agent=agent, tools=tools, verbose=True)

print(f"Welcome to my application.  I am configured with these tools")
for tool in tools:
  print(f'  Tool: {tool.name} = {tool.description}')

while True:
    try:
        line = input("llm>> ")
        if line:
            result = agent_executor.invoke({"input":line})
            print(result)
        else:
            break
    except Exception as e:
        print(e)
#chain.invoke(
